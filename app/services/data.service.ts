import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { User } from '../interfaces/user';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpclient:HttpClient) { };
  getUsers()
  {
   return this.httpclient.get("https://jsonplaceholder.typicode.com/users");
  }
  getUserDetails(id:string)
  {
    return this.httpclient.get(`https://jsonplaceholder.typicode.com/users/${id}`);
  }
  getProducts()
  {
    this.httpclient.get("");
  }
}
