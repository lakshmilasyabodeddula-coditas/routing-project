import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockDetailsComponent } from '../../product-module/product/stock-details/stock-details.component';
import { AboutComponent } from './about/about.component';
import { CompanyDetailsComponent } from './about/details/company-details/company-details.component';
import { PersonalDetailsComponent } from './about/details/personal-details/personal-details.component';
import { UserComponent } from './user.component';

const routes: Routes = [{
  path:"",
  component:UserComponent,
  children:[
    // {path:":id",
    //  component:AboutComponent},
     {path:"about/:id",
      component:AboutComponent, children: [
        {
          path:"personalDetails/:id",
          component:PersonalDetailsComponent
        },
        {
          path:"companyDetails/:id",
          component:CompanyDetailsComponent
        }
      ]
      },
    
    ]
  }]
;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
