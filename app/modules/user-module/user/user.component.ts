import { outputAst } from '@angular/compiler';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { User } from 'src/app/interfaces/user';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
 
  isAboutClicked=false;
  constructor(private data:DataService,
              private router:Router) { }
  users!:User[];
  ngOnInit(): void {
    this.data.getUsers().subscribe({
      next:(response:any)=>{
        this.users=response;
        console.log(this.users);
      }
    })
  }
  currentUserId!:number;
  // onSelect(user:User)
  // {
  //   console.log(user.id);
  //   this.router.navigate(["about",user.id]);
  //   // this.isAboutClicked=true;
  //   // this.currentUserId=user.id;
  // }
   

}
