import { AfterContentChecked, AfterContentInit, AfterViewInit, Component, DoCheck, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { User } from 'src/app/interfaces/user';
import { DataService } from 'src/app/services/data.service';
import { UserComponent } from '../user.component';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit{

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private data: DataService
  ) { }
  userid!: any;
  userDetails:any;

  isAboutClicked=false;
  ngOnInit(): void { 
    this.route.params.subscribe((params:Params)=>{
        this.userid=params['id'];
        this.data.getUserDetails(this.userid).subscribe({
          next:(response:any)=>{
            this.userDetails=response;
            console.log(this.userDetails);
          }
        })
      })
    }
  }