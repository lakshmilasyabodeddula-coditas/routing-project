import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, Params } from '@angular/router';
import { User } from 'src/app/interfaces/user';
import { DataService } from 'src/app/services/data.service';
@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {

  userDetails!:User
  constructor( private route: ActivatedRoute,
    private router: Router,
    private data: DataService) { }

  userid!:string;
  ngOnInit(): void {
    this.route.params.subscribe((params:Params)=>{
      this.userid=params['id'];
      this.data.getUserDetails(this.userid).subscribe({
        next:(response:any)=>{
          this.userDetails=response;
          console.log(this.userDetails);
        }
  })
})
  }
}
