import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { AboutComponent } from './about/about.component';

import { CompanyDetailsComponent } from './about/details/company-details/company-details.component';

@NgModule({
  declarations: [
    UserComponent,
    AboutComponent,
   
    CompanyDetailsComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
