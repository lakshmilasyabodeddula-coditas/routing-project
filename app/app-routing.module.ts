import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {path:"users",
   loadChildren:()=>import("./modules/user-module/user/user.module").then(module=>module.UserModule),
  },
   {path:"products",
    loadChildren:()=>import("./modules/product-module/product/product.module").then(module=>module.ProductModule)
  },
  {
    path:"",
    loadChildren:()=>import("./app.module").then(module=>module.AppModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
